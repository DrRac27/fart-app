# Fart App
As requested by preflex (@garythegravelguy:matrix.org) and tomz (@tomz:matrix.org) on Matrix channel #archmobile:kde.org ...

## Install (on Arch ARM)
```
cd packaging
makepkg -si
```

## Used files by other authors
- [Flatulence.wav](https://commons.wikimedia.org/wiki/File:Flatulence.wav) by JohnSumisu licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.
- [Fart.ogg](https://commons.wikimedia.org/wiki/File:Fart.ogg) by El Charpi licensed under the Creative Commons Attribution-Share Alike 4.0 International license.
- logo.png is a cropped version of [ghost fart](https://www.flickr.com/photos/80403764@N00/309185251) by banjo d which is licensed under CC BY 2.0.

## Note
This app was first written in tkinter but then rewritten in PyQt5 as tkinter didn't work on first try.
A older commit works on Arch ARM if the dependency `tk` gets installed.
