#!/usr/bin/env python3

import sys
from PyQt5.QtWidgets import QWidget, QPushButton, QApplication, QVBoxLayout
import pygame


class FartApp(QWidget):

    def __init__(self):
        super().__init__()
        pygame.mixer.init()

        vbox = QVBoxLayout()

        sbtn = QPushButton('Short Fart', self)
        sbtn.clicked.connect(self.short_fart)

        lbtn = QPushButton('Longer Fart', self)
        lbtn.clicked.connect(self.longer_fart)

        qbtn = QPushButton('Quit', self)
        qbtn.clicked.connect(QApplication.instance().quit)

        vbox.addStretch(1)
        vbox.addWidget(sbtn)
        vbox.addStretch(1)
        vbox.addWidget(lbtn)
        vbox.addStretch(1)
        vbox.addWidget(qbtn)
        vbox.addStretch(1)

        self.setLayout(vbox)

        self.show()

    def short_fart(self):
        pygame.mixer.music.load("/opt/fart-app/Fart.ogg")
        pygame.mixer.music.play()

    def longer_fart(self):
        pygame.mixer.music.load("/opt/fart-app/Flatulence.wav.ogg")
        pygame.mixer.music.play()


def main():
    app = QApplication(sys.argv)
    fart_app = FartApp()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
